worldview im;

@test(
    name = "extents.time.promoted.multiscale",
    description  = "Time-explicit process modeling",
    observations = ("hydrology:SurfaceWaterFlow"),
    // add assertions to check the observations after execution
    assertions = ()
)
observe hydrology:Watershed named simpletimegrid
    over
    	space(shape="EPSG:4326 POLYGON((33.796 -7.086, 35.946 -7.086, 35.946 -9.41, 33.796 -9.41, 33.796 -7.086))", grid=1.km),
    	time(year=2010)
;

/**
 * This, being an occurrent, will force the temporal context to "occur", 
 * i.e. graduate from logical to physical, with at least one step after 
 * initialization.
 */
@time(step=1.month)
@space(grid)
model hydrology:SurfaceWaterFlow,
	  hydrology:RunoffWaterVolume in mm/day named runoff
	observing 
		earth:Precipitation,
		geography:Elevation
	using im.hydrology.runoff() 
	do [info("FLOW time is " + time)];

@time(step=1.day)
@space(grid)
@intensive(space)
model earth:Precipitation, earth:PrecipitationVolume in mm/day
	observing earth:Storm
	do [info("PRECIPITATION time is " + time)];
	