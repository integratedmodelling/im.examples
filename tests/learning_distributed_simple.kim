worldview im;

@test(
    name = "learning.distributed.simple",
    description  = "Learn a quality within an archetype and use the learned resource for the subcontext",
    // add to the list any concepts to be observed in the context below
    observations = (""),
    // add assertions to check the observations after execution
    assertions = ()
)
// change the concept to the one you want; use a URN optionally (in a separate test project including the resource)
observe earth:Region named learning_test
    over
    	space(shape="EPSG:4326 POLYGON ((-4.3631254564618525 42.934988662846365, -2.9342822133373394 42.934988662846365, -2.9342822133373394 43.63319852564203, -4.3631254564618525 43.63319852564203, -4.3631254564618525 42.934988662846365))", grid=300.m)
;

/** 
 * Learn a quality within a distributed inherent without an archetype: the absence
 * of the archetype triggers the normal behavior of looking up the predictors within
 * the distributed countable and building the instance set from it, ignoring the 
 * context.
 */
 learn value of ecology:Biodiversity
	observing 
		@archetype earth:Site with value of ecology:Biodiversity,
		@predictor geography:Slope,
		@predictor geography:Elevation,
		@predictor earth:PrecipitationVolume
	using im.weka.bayesnet(resource = biodiversity.test.distributed);
	
// ------ the archetype --------------------------------------------
model each earth:Site with value of ecology:Biodiversity,
	slope as geography:Slope in degree_angle, 
	elevation as geography:Elevation in m,
	precipitation_volume as earth:PrecipitationVolume in mm,
	[precipitation_volume - elevation/100 - slope/90] as value of ecology:Biodiversity
  observing
  	geography:Slope in degree_angle,
	geography:Elevation in m,
	earth:PrecipitationVolume in mm
  using gis.points.extract(select = [random() > 0.999]);