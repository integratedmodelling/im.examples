private namespace im.attributes;

/*
 * Example of using attribute and role dependencies (the same logics work as queries
 * typed in the Explorer, which are dependencies of the context). 
 * 
 * The following are the legal usages: 
 * 
 * 	<abstract trait/role> of <countable> 
 *  <concrete trait/role> <countable>.
 * 
 * Both <abstract trait> <object> and <concrete trait> of <object> are illegal 
 * dependencies.
 * 
 * NONE OF THESE WORK at this stage!
 * 
 */
model earth:Region

	observing
	
		/**
		 * The attribute is ABSTRACT: this means find all sites for which an orientation can
		 * be determined, classify them with a CONCRETE orientation, then resolve the sites
		 * and the orientation, in sequence.
		 * 
		 * This dependency can be handled directly by a model of 'earth:Site with im:Orientation', which would
		 * instantiate the sites with the attribute. If that's not available, a model of 'each earth:Site' and 
		 * one of 'im:Orientation of each earth:Site' will be looked up. In that last case, if the dependency is 
		 * not optional, any Site found that cannot be classified by orientation is removed. If it's optional, 
		 * it means having the attribute is optional for the sites, and they are kept.
		 * 
		 * After instantiation and attribution, each Site has a concrete Orientation trait, and a resolver will
		 * be looked for to resolve the attribute in each site: first k.LAB will try to resolve the CONCRETE 
		 * orientation (e.g. with 'model im:East earth:Site') and if not found, it will try a generic 
		 * 'model im:Orientation of earth:Site'. None of these will be mandatory: if they are not found, the
		 * Site and its attribute stay where they are.
		 * 
		 * Using 'of' here is mandatory, although it's just for fluency and to enforce user awareness of what
		 * the dependency is about.
		 */
		im:Orientation of earth:Site,
	
		/** 
		 * The dependency is for a countable (MountainPeak) classified with a CONCRETE
		 * role (an attribute would work the same way). This can be satisfied directly by a 
		 * 'model each es.aesthetics:AestheticallyValuable earth:MountainPeak'. Otherwise,
		 * k.LAB will: 
		 * 
		 * 		1) find all mountainpeaks ('model each MountainPeak'); if found
		 * 		2) classify them with the abstract base role corresponding to AestheticallyValuable 
		 *        (using its implicit parent if it's a deniable trait) using a role instantiator; 
		 *        if not found, resolution fails; 
		 *      3) remove any non-aesthetically valuable mountain peaks from the context;  
		 *      4) resolve the role in each with an optional 'model es.aesthetics:AestheticallyValuable earth:MountainPeak'. 
		 * 
		 * If the dependency is optional, Mountainpeaks that are not aesthetically valuable do NOT remain in the 
		 * context; simply the failure of this resolution does not stop the model from running, as it normally would.
		 * 
		 * Compared to roles, attributes have implications that are context-dependent, and resolving one may
		 * trigger instantiation and resolution of other roles in other observables, depending on semantics.
		 * 
		 * Using 'of' is incorrect here, for the same reasons above.
		 */
		es.aesthetics:AestheticallyValuable earth:MountainPeak;

/** 
 * The following are examples of role/attribute instantiators and resolvers, necessary for
 * the resolution of the example above. Instantiators convey the attribute or role (they are
 * CLASSIFIERS). Resolvers come in after the attribute/role has been conveyed by an instantiator
 * and they may delete the object like any object resolver.
 */

// abstract trait observable with propagated inherency - this requires specific detection, not a concept
// "(of|for|within) each" should be treated as a modifier that can only be used within the observable of a model.
// ATTRIBUTION ABSTRACT INSTANTIATOR
model im:Orientation of each earth:Site;

// resolver: each site with any level is resolved according to the model. Sites must exist
// abstract trait observable with inherency. Can say 'within' if the resolution is for a
// concrete trait. Again, the operator is for fluency and awareness.
// ATTRIBUTION ABSTRACT RESOLVER
model im:Orientation of earth:Site;

/**
 * The resolver for a concrete trait or role can simply add it to the subject; the subject at this
 * point must exist, and if it has the trait this will be found. It can use a more generic subject 
 * type, including an abstract one, to apply to more types.
 * // ATTRIBUTION CONCRETE RESOLVER
 */
model es.aesthetics:AestheticallyValuable earth:Site;

/**
 * In this case, the observable is the concrete attribute, which is made inherent to
 * the abstract quality. It will be used to resolve the attribute within any of the
 * compatible inherents.
 * //  ATTRIBUTION CONCRETE RESOLVER
 */
model im:Normalized im:Quantity;